/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.factorization.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 *Обработчик клика с созданием новой строки для ввода числа
 * @author alex
 */
public class ButtonClickHandler implements ClickHandler{

    private boolean clicked = false;
    private final VerticalPanel panel;
    
    /**
     * Конструктор принимает параметром панель, для добавления новой строки
     * @param panel - Вертикальная панель
     */
    public ButtonClickHandler(VerticalPanel panel) {
        this.panel = panel;
    }
    
    /**
     * Обработка клика
     * @param event 
     */
    @Override
    public void onClick(ClickEvent event) {
        if (!clicked) {
            InputFields nfld = new InputFields(new ButtonClickHandler(panel));
            panel.add(nfld);
            clicked = true;
        }
    }
    
}
