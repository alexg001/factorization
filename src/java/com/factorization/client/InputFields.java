/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.factorization.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.widget.client.TextButton;
import java.awt.event.KeyEvent;

/**
 *Виджет для отображения на сайте
 * @author alex
 */
public class InputFields implements IsWidget {
    
    private final TextButton button = new TextButton("Множители");
    private final TextBox textBox = new TextBox();
    private final Label resultLabel  = new Label("");
    private final ClickHandler clickHandler;
    
    /**
     * Конструктор принимает внешний обработчик нажатия кнопки
     * @param clickHandler - обработчик нажатия кнопки
     */
    public InputFields(ClickHandler clickHandler) {
        this.clickHandler = clickHandler;
        initializatioutton();
    }
    /**
     * Инициализация виджетов
     */
    private void initializatioutton() {
        this.button.setStyleName("btn");
        this.textBox.setStyleName("txt");
        //Добавление обработчика нажатия кнопки в текстовом поле
        this.textBox.addKeyPressHandler(new KeyPressHandler() {

            @Override
            public void onKeyPress(KeyPressEvent event) {
                
                if (!(("0123456789".contains(String.valueOf(event.getCharCode()))) || (event.getNativeEvent().getKeyCode() == KeyEvent.VK_BACK_SPACE))) {
                    textBox.cancelKey();
                }
            }
        });
        this.resultLabel.setStyleName("res");
        //Добавление обработчика нажатия кнопки
        this.button.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                //Вызов события нажатия кнопки во внешнем обработчике
                clickHandler.onClick(event);
                //Проверка введенных сведений
                long number = 0;
                try {
                    number = Long.parseLong(textBox.getText());
                    if (number < 0) {
                        throw new Exception();
                    }
                } catch (Exception ex) {
                    resultLabel.setText("Число указано некорректно. Число должно быть в диапазоне от 0 до 2^63-1");
                    resultLabel.setStyleName("err");
                    return;
                }
                //Подготовка запроса на сервер
                final AsyncCallback<Long[]> callback = new AsyncCallback<Long[]>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        resultLabel.setText("Ошибка получения данных от червера");
                        resultLabel.setStyleName("err");
                    }

                    @Override
                    public void onSuccess(Long[] result) {
                        //Формирование строки ответа сервера
                        String res = "Ответ сервера: ";
                        for(Long n : result) {
                            res = res + n.toString() + " * ";
                        }
                        res = res.substring(0, res.length() - 3);
                        //Отображение строки
                        resultLabel.setStyleName("res");
                        resultLabel.setText(res); 
                    }
                };
                //отправка запроса на сервер
                getService().factorization(number, callback);
            }
        });
    }
    
    @Override
    public Widget asWidget() {
        //Подготовка панели с виджетами
        HorizontalPanel panel = new HorizontalPanel();
        panel.add(this.textBox);
        panel.add(this.button);
        panel.add(this.resultLabel);
        panel.setStyleName("row");
        return panel;
    }
    
    /**
     * Метод для получения указателя на службу сервера
     * @return - указатель службы
     */
    public static FactorizationServiceAsync getService() {
        return GWT.create(FactorizationService.class);
    }
    
}
