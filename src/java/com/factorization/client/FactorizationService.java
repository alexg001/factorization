/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.factorization.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 *Интерфейс службы сервера
 * @author alex
 */
@RemoteServiceRelativePath("factorizationservice")
public interface FactorizationService extends RemoteService {

    public Long[] factorization(long number);
}
