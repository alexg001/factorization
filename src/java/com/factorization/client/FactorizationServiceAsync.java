/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.factorization.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *Асинхронный интерфейс службы сервера
 * @author alex
 */
public interface FactorizationServiceAsync {

    public void factorization(long number, AsyncCallback<Long[]> callback);
}
