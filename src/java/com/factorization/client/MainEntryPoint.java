/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.factorization.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Класс с точкой входа
 *
 * @author alex
 */
public class MainEntryPoint implements EntryPoint {

    private final VerticalPanel panel = new VerticalPanel(); //основная панель
            
    public MainEntryPoint() {
    }

    /**
     * точка входа
     */
    @Override
    public void onModuleLoad() {
        //Добавляем первую строчку на страницу
        InputFields fld = new InputFields(new ButtonClickHandler(panel));
        panel.add(fld);
        RootPanel.get("content").add(panel);
    }
}
