/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.factorization.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import com.factorization.client.FactorizationService;

/**
 *Реализация службы сервера
 * @author alex
 */
public class FactorizationServiceImpl extends RemoteServiceServlet implements FactorizationService {

    /**
     * Метод для определения множителей сервером
     * @param number - искомое число
     * @return - массив множителей для передачи клиенту
     */
    @Override
    public Long[] factorization(long number) {
        Long[] res = FactorizationClass.factorization(number);
        return res;
    }
    
}
