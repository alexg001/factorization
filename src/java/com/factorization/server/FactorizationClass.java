/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.factorization.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *Разложение числа на множители по алгоритму Полларда
 * @author alex
 */
public class FactorizationClass {

    /**
     * Метод, возвращающий для двух чисел их наибольший общий делитель:
     * @param a - первое число
     * @param b второе число
     * @return  наибольший делитель
     */
    public static long nod(long a, long b) {
        while (b != 0) {
            long r = a % b; // Остаток от деления a на b.
            // Перезапись переменных и дальнейшее повторение действия.
            a = b;
            b = r;
        }
        return a;
    }

    /**
     * Функция определяет делитель числа
     * @param number - исходное число
     * @return - делитель числа
     */
    public static long getFactor(long number) {
        //Проверка на квадрат
        if ((long) Math.sqrt(number) == Math.sqrt(number)) {
            return (long) Math.sqrt(number);
        }
        //Проверка на кратность 2
        if (number % 2 == 0) {
            return 2;
        }
        long nod = 1;
        int step = 1;
      
        long x = 2;
        long y = 2;
        //Вычисление делителя
        while (nod <= 1) {
            x = getFunctionResult(x, 1, number);
            y = getFunctionResult(y, 2, number);
            nod = nod(number, Math.abs(x - y));
            step++;
        }
        return nod;
    }

    /**
     * Расчет числа для определенного шага
     * @param X0 - исходное число для итерации (расчитанное ранее)
     * @param stepNumber - количество итераций расчета
     * @param originalNumber - исходное число
     * @return - результат расчета
     */
    private static long getFunctionResult(long X0, int stepNumber, long originalNumber) {

        long x = X0;   //Начальное значение
        for (int i = 0; i < stepNumber; i++) {
            x = ((x * x) - 1) % originalNumber;
        }
        return x;
    }

    /**
     * Функция определяет простые множители числа
     * @param number - исходное число
     * @return - массив множителей
     */
    public static Long[] factorization(long number) {
        List<Long> result = new ArrayList<Long>(3); // для результата
        long factor = 0;
        long stepNumber = number; 
        //Цикл определения делителей с рекурсивным вызовом
        while (factor <= stepNumber) {
            factor = getFactor(stepNumber);
            if (factor < stepNumber) {
                result.addAll(Arrays.asList(factorization(factor)));
            } else {
                result.add(factor);
            }
            stepNumber = stepNumber / factor;
        }
        //Сортировка и возврат множителей
        Collections.sort(result); 
        return result.toArray(new Long[]{});
    }
}
