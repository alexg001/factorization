/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.factorization.server.FactorizationClass;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *Тестирование алгоритма разложение на множители
 * @author alex
 */
public class factorizationTest {
    
    public factorizationTest() {
    }

    @Test
    public void factirizationTest() {
        Long[] res = FactorizationClass.factorization(123);
        Assert.assertArrayEquals(new Long[] {new Long(3), new Long(41)}, res);
        Long[] res1 = FactorizationClass.factorization(657577);
        Assert.assertArrayEquals(new Long[] {new Long(17), new Long(47), new Long(823)}, res1);
        Long[] res2 = FactorizationClass.factorization(49);
        Assert.assertArrayEquals(new Long[] {new Long(7), new Long(7)}, res2);
        Long[] res3 = FactorizationClass.factorization(857657674684356L);
        Assert.assertArrayEquals(new Long[] {new Long(2), new Long(2), new Long(3), new Long(7), new Long(13), new Long(41243), new Long(19043251)}, res3);
        Long[] res4 = FactorizationClass.factorization(3571);
        Assert.assertArrayEquals(new Long[] {new Long(3571)}, res4);
        Long[] res5 = FactorizationClass.factorization(2412138756707L);
        Assert.assertArrayEquals(new Long[] {new Long(563), new Long(4284438289L)}, res5);
        Long[] res6 = FactorizationClass.factorization(9223372036854775807L);
        Assert.assertArrayEquals(new Long[] {new Long(7), new Long(7), new Long(73), new Long(127), new Long(337), new Long(92737), new Long(649657)}, res6);
    }
}
